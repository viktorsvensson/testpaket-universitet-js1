Ett väldigt litet testpaket för att kunna självtesta de grundläggande kraven för universitetshemsideprojektet.

För att köra

- "node -v"

(Verifierar att node är installerat och finns tillgängligt genom att skriva ut versionsnummret i så fall, annars installera / felsök)

- npm install

(installerar externa dependencies)

- npx cypress open

(Startar Cypress)

Se även särskilt instruktionsklipp