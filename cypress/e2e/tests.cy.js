/// <reference types="cypress"/>

describe('Projekttest', () => {

  let cssUrl;

  before(() => {
    cy.visit('localhost:5500')
    cy.get('link[href*="css"]')
      .invoke('attr', 'href')
      .then(url => {
        cssUrl = "/" + url;
      })
  })

  beforeEach(() => {
    cy.visit('localhost:5500')
  })

  //GRID
  it('Should have css display: grid', () => {
    cy.request(cssUrl)
      .its('body')
      .should('match', /display:\s*grid/)
  })

  //RESPONSIVE
  it('Should use media-queries for responsivness', () => {
    cy.request(cssUrl)
      .its('body')
      .should('contain', '@media')
  })

  //ANIMATIONS
  it('Should use keyframes for two animations', () => {
    cy.request(cssUrl)
      .its('body')
      .should('match', /@keyframes(\S|\s)*transform: | @keyframes(\S|\s)*@keyframes | transform:(\S|\s)*transform | transform:(\S|\s)*@keyframes/)
  })

  //FORM, SUBMIT
  it('Should have a form', () => {
    cy.get('form').should('exist')
  })

  it('Should have a input', () => {
    cy.get('input').should('exist')
  })

  it('Should be able to submit form with a button', () => {
    cy.get('button[type="submit"]').should('exist')
  })

  //SEO
  it('Should have a header element', () => {
    cy.get('header').should('exist')
  })

  it('Should have a main element', () => {
    cy.get('main').should('exist')
  })

  it('Should have a footer element', () => {
    cy.get('footer').should('exist')
  })

  it('Should have a h1 in header', () => {
    cy.get('header').find('h1').should('exist')
  })

  it('Should have only one h1', () => {
    cy.get('h1').its('length').should('eq', 1)
  })

  it('Should have at least one h2', () => {
    cy.get('h2').should('exist')
  })

  it('Should have "sv" language attribute', () => {
    cy.get('html[lang="sv"]').should('exist')
  })

  it('Should use the word "Universitet(et)', () => {
    cy.contains('universitet', { matchCase: false })
  })
})

